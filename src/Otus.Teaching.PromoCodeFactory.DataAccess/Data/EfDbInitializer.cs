﻿using System.Linq;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public class EfDbInitializer : IDbInitializer
    {
        private readonly DataContext _dataContext;

        public EfDbInitializer(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public void InitializeDb()
        {
            _dataContext.Database.EnsureDeleted();
            _dataContext.Database.EnsureCreated();

            _dataContext.Employees.AddRange(FakeDataFactory.Employees);
            _dataContext.SaveChanges();

            _dataContext.Roles.AddRange(FakeDataFactory.Roles.Where(role=>!_dataContext.Roles.Select(r=>r.Id).Contains(role.Id)));
            _dataContext.SaveChanges();

            _dataContext.Customers.AddRange(FakeDataFactory.Customers);
            _dataContext.SaveChanges();

            _dataContext.Preferences.AddRange(FakeDataFactory.Preferences.Where(p => !_dataContext.Preferences.Select(r => r.Id).Contains(p.Id)));
            _dataContext.SaveChanges();

            _dataContext.CustomerPreferences.AddRange(FakeDataFactory.CustomerPreferences);//.Where(p => !_dataContext.Preferences.Select(r => r.Id).Contains(p.Id)));
            _dataContext.SaveChanges();

            
        }
    }
}
