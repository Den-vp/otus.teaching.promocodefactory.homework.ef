﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class EfRepository<T> : IRepository<T>
        where T : BaseEntity
    {
        private readonly DataContext _dataContext;

        public EfRepository(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public async Task AddAsync(T entity)
        {
            await _dataContext.Set<T>().AddAsync(entity);
            await _dataContext.SaveChangesAsync();
        }

        public async Task DeleteAsync(T entity)
        {
            _dataContext.Set<T>().Remove(entity);
            await _dataContext.SaveChangesAsync();
        }

        public async Task<IEnumerable<T>> GetAllAsync() => await _dataContext.Set<T>().ToListAsync();

        public async Task<T> GetByIdAsync(Guid id) => await _dataContext.Set<T>().FirstOrDefaultAsync(x => x.Id == id);

        public async Task<IEnumerable<T>> GetWhere(Expression<Func<T, bool>> expression) => await _dataContext.Set<T>().Where(expression).ToListAsync();

        public async Task UpdateAsync(T entity) => await _dataContext.SaveChangesAsync();
    }
}
